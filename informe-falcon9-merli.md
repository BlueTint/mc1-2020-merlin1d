
# Vehículo de lanzamiento Falcon 9 #

Falcon 9 es un cohete de 2 etapas diseñado y fabricado por la compañía privada SpaceX. Su propósito es el transporte confiable y seguro de satélites, de la nave espacial Dragon (otro proyecto de la misma empresa) y de personas, además es el primer cohete que puede ser reutilizado. La primera etapa puede volver a aterrizar y volver a usarse, mientras que la segunda se descarta en cada lanzamiento. 

<center>
<img src="https://upload.wikimedia.org/wikipedia/commons/b/b4/Spaceflight_SSO-A_Mission_%2845257570925%29.jpg" width="350" height="488"> 

> Falcon 9 despegando

</center>

## Descripción y versiones ##

Puede llevar 22.800 kg de carga a una órbita terrestre baja (LEO) y 8.300 kg a una órbita geoestacionaria (GTO), se calcula que podrá llevar 4.020 kg a Marte. La primera etapa cuenta con 9 motores Merlín y un tanque de aleación de aluminio y litio, que contiene oxígeno líquido y propulsor de queroseno de grado cohete (RP-1). La segunda etapa es impulsada por un solo motor de vacío Merlín, se encarga de entregar la carga útil del Falcon 9 a la órbita correspondiente. Entre la etapa primera y segunda se encuentra la etapa intermedia, una estructura en la que se encuentra los empujadores neumáticos que permiten que las etapas se separen durante el vuelo.

A continuación se presenta un cuadro con las versiones del Falcon 9 y sus especificaciones:

| Versión               |	v1.0      |	v1.1        | v1.2 Block 3 y Block 4  | v1.2 Block 5            |
| :-------------------: | :---------: | :---------: | :---------------------: | :---------------------: |
| Estado                | retirado    | retirado    |	retirado              |	activo                  |
| Motores de la etapa 1 | 9 Merlín 1C | 9 Merlín 1D | 9 Merlín 1D Actualizado | 9 Merlín 1D Actualizado|
| Motores de la etapa 2 | 1 Merlín 1C Vacuum | 1 Merlín 1D Vacuum | 1 Merlín 1D Vacuum mejorado | 1 Merlín 1D Vacuum actualizado |
| Altura máxima [m] | 53 | 68,4 | 70 | 70 |
| Diámetro [m] | 3,66 | 3,66 | 3,66 | 3,66 |
| Masa [kg] | 318 | 506 | 549 | 549 |
| Empuje inicial [kN] | 3.807 | 5.885 | 6.804 | 7.600
| Carga útil a LEO [kg] | 8500 - 9000 |	13.150 | 22.800(prescindible) |	22.800(prescindible) 16.800(reutilizable) |
| Carga útil a GTO [kg] | 3.400 | 4.850 | 8300(prescindible) 5.300(reutilizable) | 8.300(prescindible) 5.800(reutilizable) |
| Primer vuelo | 2010 | septiembre de 2013 | diciembre de 2015 | mayo de 2018 |

Tanto el modelo v1.0 y el v1.1 no podían ser reutilizados, eran prescindibles. Recién se logró en la versión v1.2 que la etapa 1 sea reutilizable.

## Sistema de Aterrizaje ##

La recuperación de la primera etapa del Falcon 9 se logra mediante un aterrizaje vertical utilizando retropropulsión supersónica. El aterrizaje puede ser de dos tipos según la superficie en la que se aterrice, sobre una barcaza en el mar o en el lugar de lanzamiento (LTRS). El sistema de aterrizaje que se utiliza en cada caso es esencialmente igual.

Luego de la separación de las etapas primera y segunda, se procede en los siguientes pasos:

* **Flip maneuver:** "maniobra de volteo". Se encienden los propulsores laterales de nitrógeno, los cuales giran la primera etapa haciendo  que cambie su orientación 180°.

* **Boostback burn:** “encendido del empuje hacia atrás”. Se encienden de nuevo algunos de los motores principales (generalmente 3) para orientar la trayectoria hacia el lugar de aterrizaje. Este paso dura unos pocos segundos. 

* **Grid fins deploy:** "despliegue de aletas de rejilla". Una vez que se apagan los motores, durante un momento el vehículo se encuentra fuera de las capas densas de la atmosfera, en esta instancia despliega las rejillas aerodinámicas al mismo tiempo que mantiene la orientación gracias a los propulsores laterales.

* **Entry burn:** “encendido de entrada”. Con el cohete ya en trayectoria descendente y apuntando los propulsores hacia tierra, se enciende uno de los motores Merlín 1D en el caso del aterrizaje sobre una barcaza y tres en el caso de un aterrizaje LTRS durante unos pocos segundos, con el objetivo de disminuir la velocidad y suavizar la entrada a la atmosfera.

* **Aerodynamic guidance:** “orientación aerodinámica”. Ya en la atmosfera las rejillas de titanio ubicadas en la base de la etapa intermedia, se encargan tanto de frenar el vehículo por rozamiento como de mantener la trayectoria necesaria para el aterrizaje.

* **Landing burn:** “encendido de aterrizaje”. Finalmente, a unos pocos kilómetros de la superficie se realiza el último encendido de los motores, para reducir en pocos segundos la velocidad de la primera etapa a cero justo en el momento de tocar la superficie.

Como la potencia mínima proporcionada por un motor Merlín 1D es capaz de elevar la primer etapa vacía, esta no puede levitar, por lo que la electrónica y el software de control deben calcular perfectamente la velocidad y posición del vehículo para ajustar la potencia y el tiempo de encendido de manera que la velocidad sea exactamente cero cuando se encuentre justo sobre la superficie en la que se quiere aterrizar.

## Evolución del motor de cohete Merlin
La familia de impulsores para cohetes Merlín, desarrollados por la empresa SpaceX y utilizados en los vehiculos de lanzamientos Falcon 1, Falcon 9 y Falcon Heavy, han sido revolucionarios, gracias a capacidad que tienen para ser recuperados y reutilizados luego de una misión.
Obviamente los motores de cohetes Merlín se han fabricado y perfeccionado a lo largo del tiempo, teniendo diferentes versiones, que van desde la 1A (la primera versión utilizada) hasta la 1D Vacuum (la que se usa actualmente).
### Merlin 1A
Fue la primera versión de estos propulsores. Este utilizaba una boquilla de polimero reforzado con fibra de carbono, y podía ejercer hasta 340 kN de empuje.  El Merlin 1A solo fue utilizado en dos ocasiones: la primera vez fue el 24 de marzo de 2006, cuando se incendió y falló a causa de una fuga de combustible poco tiempo después del lanzamiento, y luego el 21 de marzo de 2007, en este caso, la misión fue exitosa. En ambas veces el motor fue utilizado para la primera etapa del proyecto Falcon 1. 

<center>

<img src="http://www.b14643.de/Spacerockets/Specials/U.S._Rocket_engines/Merlin-1A.jpg" width="500" height="470">


> Merlin 1A

</center>

### Merlin 1B
Este motor fue una mejor mejorada del Merlin 1A.  Fue llevado a cabo gracias a la compañia Barber-Nichols Inc., que trabajó para SpaceX. Al igual que la versión 1A, se diseño especialmente para trabajar con el proyecto Falcon 1. La novedad que presentó esta versión es que una mejora en la turbina logró aumentar su potencia de 1500 kW a 1900 kW. Esta nueva actualización permitía generar una fuerza de empuje de 380 kN a nivel del mar y hasta 420 kN en el vacío, y también funcionar con un impulso específico de 261 s a nivel del mar y 303 s en el vacío. 
El motor 1B en teoría iba a ser utilizado en los vehículos de lanzamiento Falcon 9, pero debido a las experiencias sufridas en el primer vuelo del Falcon 1, SpaceX prefirió pasar su desarrollo Merlin a la versión 1C, que puede enfriarse de forma regenerativa. 
### Merlin 1C
Este motor usa una tobera y una cámara de combustión que se enfría por regeneración, y completó su prueba de ignición (de 170 segundos) en noviembre del 2007.
Cuando se utilizó para los vehículos del tipo Falcon 1, la versión 1C permitía un empuje a nivel del mar de 350 kN, un empuje en el vacío de 400 kN y un impulso específico de 304 segundos. El motor consumía alrededor de 140 kg de propelente combustible por segundo. Las pruebas exitosas de este motor han funcionado correctamente 27 minutos (sumando la duración de varias pruebas).
Cuando fue utilizado para los nuevos vehículos Falcon 1e y Falcon 9, su empuje a nivel del mar fue de 560 kN y su impulso específico de 300 segundos.
#### Merlin 1C Vacuum
En marzo de 2009, SpaceX mediante un comunicado de prensa anunció que realizaron pruebas exitosas del motor Merlin 1C Vacuum. Este era una variación del motor 1C, presentaba una sección de escape más grande y una boquilla de expansión amplia, para poder maximizar la eficiencia del motor en el vacío. Ofrecía un empuje en el vacío de 411 kN y un impulso específico de vacío de 342 segundos.
### Merlin 1D
Desarrollado entre los años 2011 y 2012 por SpaceX, el Merlin 1D tuvo una capacidad de empuje de 620 kN a nivel del mar y de 690 kN en el vacío, y un impulso específico de 310 segundos.
El novedoso diseño que presentaba este motor incluía un aumento de la vida útil de la fatiga y de los márgenes térmicos de la cámara y boquilla, un mejor rendimiento (al aumentar su capacidad de empuje tanto a nivel del mar como en el vacío) y también mayor rentabilidad, al poseer un menor número de piezas que sus anteriores versiones, habían que invertir menos horas de trabajo para su producción.
Su primera prueba fue completada exitosamente, con una duración de 185 segundos de lanzamiento de 650 kN de empuje. Este motor tiene el impulso específico más alto jamás alcanzado para un motor del generador del gas-cohete del motor del cohete. En junio de 2013 el vehículo de vuelo orbital Falcon 9 1.1 primera etapa, completo con exito su misión utilizando el Merlin 1D.
El primer vuelo del Falcon 9 con este tipo de motores lanzó el satélite CASSIOPE perteneciente a la Agencia Espacial Canadiense. Con un peso de 360 kg, el CASSIOPE fue lanzado en una órbita terrestre baja. El segundo vuelo fue el lanzamiento de la órbita de transferencia geosincrónica (GTO) de SES-8.

<center>

<img src="http://www.b14643.de/Spacerockets/Specials/U.S._Rocket_engines/Merlin-1D.jpg" width="500" height="470">


> Merlin 1D

</center>

#### Merlin 1D Vacuum
Al igual que en la versión 1C, también se desarrolló una versión de vacío para el Merlin 1D, para ser implementado en el Falcon 9 v1.1 y en la segunda etapa del Falcon Heavy. Con esta mejora el empuje del Merlin 1D Vacuum es de 934 kN con un impulso específico de 348 segundos, siendo el mayor impulso específico para un motor de cohete de hidrocarburos de los Estados Unidos. Este logro se debe a una mayor relación de expansión al operar en vacío, utilizando una extensión de boquilla. El motor puede acelerar hasta el 29% de su empuje máximo.

<center>

<img src="http://www.b14643.de/Spacerockets/Specials/U.S._Rocket_engines/Merlin-1DV.jpg" width="500" height="470">


> Merlin 1D Vacuum

</center>

## Motores Merlin 1D
Las versiones actuales de la familia de motores Merlin 1D y 1D Vacuum fueron desarrolladas entre los años 2011-2013. Su diseño se basa en que las máquinas sean confiables y tengan mejor rendimiento, además de tratar de aumentar las capacidades de fabricación de las mismas.
<center>
<img src="https://i.blogs.es/e73c0b/merlin1d/1366_2000.jpg" width="350" height="488"> 

> Motor Merlin 1D N° 400

</center>

Entre las mejoras con respecto a las versiones anteriores se puede mencionar la simplificación de diseño mediante la eliminación de componentes y subconjuntos, un aumento en la vida de fatiga y un aumento de los márgenes térmicos de la cámara de combustión y de la boquilla. Las mejoras incorporadas fueron aumentando el rendimiento de tal manera que actualmente el empuje a nivel del mar del motor Merlin 1D es de 845 kN y el impulso específico (Isp) una medida de cuán eficiente es el uso de combustible, es de 282 seg (ó 2.77 km/s); y para la versión 1D Vacuum el empuje es de 981 kN con un Isp de 311 seg (ó 3.05 km/s). Ambas versiones se pueden reutilizar hasta 10 veces y en el caso de los 9 motores del Falcon 9, si alguno llegase a fallar los motores restantes pueden seguir funcionando y terminar la misión. El costo de los motores Merlin es menor a 1 millón de dólares.

### Diseño y principio de funcionamiento  
#### Control del motor 
Para contrarrestar efectos de radiación (Bit Flip) y cualquier error que pueda surgir, en su diseño triple redundante y tolerante a fallas, Spacex utiliza 3 computadoras por cada motor Merlin para el sistema de control. Los procesadores de las 3 computadoras se verifican constantemente entre sí, para que en el caso de que algún cálculo en uno de los procesadores sea incorrecto se pueda corregir al comparar con los demás procesadores. Los componentes utilizados para el sistema de control son estándar lo que permite reducir costos y facilitar su uso, a diferencia de los componentes específicos endurecidos por radiación.
#### Partes principales
<center>
<img src="https://i.pinimg.com/originals/f9/9d/6e/f99d6e99c82968ab368061f3f3872935.jpg" width="500" height="470"> 

> Esquema de un motor Merlin

</center>

La cámara de combustión y la boquilla en conjunto forman la cámara de empuje. Debido a que estas partes son las que resultan más expuestas a altas temperaturas su enfriamiento es por regeneración, es decir el combustible (criogenizado) fluye en espiral dentro de las "paredes" rodeando la boquilla y la cámara de combustión. Sobre la cámara de combustión se encuentra un inyector de pinza, que se encarga de mezclar cantidades óptimas de oxidante y de combustible a medida que entran a la cámara para que el proceso de combustión sea controlado y eficiente.  
A un lado de la cámara de combustión se encuentran dos bombas: una para el oxidante (LOX) y otra para el combustible (RP-1), que se encargan de que llegue el flujo de masa adecuado con una presión apropiada al inyector de pinza. Ambas bombas están conectadas por un único eje a una turbina impulsora (denominada turbobomba) que a su vez es impulsada por un generador de gas (GG).  
El GG hace una combustión del LOX y RP-1 para obtener la energía, en forma de gas caliente, que enciende las turbobomba; es por esto que el motor Merlin es bipropelente (también llamado de ciclo abierto): parte del propelente se quema para alimentar las bombas del motor, y el gas caliente generado se "drena" (desecha). Aunque luego de que el gas pasa por la turbobomba se recicla en la entrada de baja presión para utilizarlo en los actuadores hidráulicos, componentes que controlan el movimiento de la boquilla para dirigir el empuje y así controlar la altitud y la velocidad angular del cohete. Esto permite que no sea posible la falla de control en la vectorización de empuje por falta de fluido hidráulico.
#### Funcionamiento
El RP-1 y LOX llegan a sus respectivas bombas, y salen a alta presión gracias a la turbobomba. El LOX va directamente al inyector, pero el RP-1 primero recorre los canales para el enfriamiento antes de llegar. Una vez que ocurre la combustión de los líquidos, el gas resultante se acelera al pasar por la "garganta" de la cámara y al llegar a la boquilla se expande y se genera el empuje que mueve el cohete (acción-reacción).
 
## ¿Qué es un inyector de pinza?
El inyector de pinza o aguja (*Pintle Inyector*, en inglés) es un tipo de inyector de propulsión para el motor de un cohete bipropelente de combustible líquido. Como cualquier otro inyector, su objetivo es el de garantizar el caudal adecuado y mezcla de los propulsores, para que ocurra una combustión eficiente y controlada.
Este inyector puede tener un rango de aceleración mucho más grande que uno regular, y también es poco frecuente que presente inestabilidades de combustión acustica, ya que un inyector de pinza tiende a formar un patrón de flujo autoestabilizante. Por este motivo, los motores que utilizan este tipo de pinza son bastante adecuados para aplicaciones que requieren una aceleración profunda, rápida y segura, como lo es el momento de aterrizaje de un cohete.

<center>

<img src="https://upload.wikimedia.org/wikipedia/commons/1/10/Pintle_injector%2C_inner_and_outer_flow.png" width="500" height="470">


> Un inyector de pinza en una prueba de flujo en frio

</center>


### Funcionamiento
Un inyector de pinza es un tipo de inyector coaxial (tiene el mismo eje de simetría que otro cuerpo). Dos tubos concentricos con una protuberancia central lo conforman. 
El propelente A (generalmente es el oxidante, de color *azul*) fluye a través de un tubo externo, que sale como una corriente cilíndrica, mientras que el propelente B (generalmente es el combustible, de color *rojo* ) fluye a través de un tubo interno y choca contra el centro de un pivote. La pulverización del propelente A genera un cono amplio que se cruza con la corriente cilíndrica del propelente B.
Los motores que utilizan este tipo de inyectores, solo usan un inyector central. Que sea un inyector central se refiere a que el flujo es despedido unicamente por un único orificio central, y no por varias salidas como ocurre en los inyectores del tipo de "puertos paralelos"
La aceleración puede regularse colocando válvulas antes del inyector o moviendo el pivote.

<center>

<img src="https://upload.wikimedia.org/wikipedia/commons/1/1f/Pintle_3.png" width="350" height="488"> 



> Corte longitudinal de un inyector de aguja o pinza

</center>


### Uso 
Este tipo de inyectores viene utilizándose desde la década de 1970 en distintos motores del tipo bipropelente. Actualmente (desde los años 2010), el único motor que utiliza los inyectores de aguja es el Merlín. Este motor se encuentra en todos los vuelos realizados por SpaceX, como el Falcon 9 y el Falcon Heavy.

### Pros y contras
#### Pros
- **Aceleración**: Estos inyectores permiten una aceleración mucho más significativa para los caudales tipo bipropelentes, en comparación a los inyectores de otro tipo. Al utilizar solo un inyector central, el flujo de masa dentro de la cámara de combustión tendrá solo dos zonas de circulación, por lo tanto, la inestabilidad acústica baja, sin la necesidad de equipar al motor con cavidades o deflectores acústicos.
- **Eficiencia**: El diseño de un inyector de aguja tiene una gran eficiencia de combustión, generalmente rondando el 96 a 99%.
- **Combustible**: El inyector puede ajustar cualquier exceso de combustible que no reaccione, al pasar inmediatamente a través de la corriente del oxidante.
- **Adaptabilidad**: En un principio, estos inyectores se han fabricado principalmente para la propulsión de cohetes, pero debido a su simpleza, estos podrían adaptarse en procesos industriales de manejo de fluidos que necesitan de gran caudal y una mezcla completa.
- **Manobriabilidad**: Como el rendimiento de un inyector depende principalmente de la forma que tenga el pivote que se encuentra en la zona de salida, probar con variaciones en el rendimiento suele ser menos costoso y rápido, al solo necesitar la fabricación de dos piezas nuevas para cada prueba.
#### Contras
- **Esfuerzo térmico**: Los esfuerzos térmicos tienden a incrementarse en la pared de la combustión, de manera no uniforme. Esto se debe tener en cuenta al diseñar el sistema de enfriamiento, o podría ocurrir un sobrecalentamiento del inyector.
- **Erosión**: En los primeros motores Merlín, los inyectores de pinza han erosionado la garganta del motor, debido a una mezcla desigual que genera rayas calientes en el flujo. Aunque a partir de 2019 no está claro si esto ocurre solo en los motores tipo Merlin o todos los que utilizan los inyectores de este tipo.
- **Propulsores**: Este tipo de inyector funciona correctamente con propulsores del tipo líquido, e incluso con aquellos que son gelificados. Pero para los propulsores gas-líquido, gas-gas, los inyectores convencionales son superiores con respecto al rendimiento.
- **Mezcla**: Estos inyectores son ideales para los motores que deben ser acelerados o reiniciados varias veces, pero su eficiencia no es óptima para el caso de mezcla de combustible y oxidante a cualquier velocidad y aceleración dada, solo para determinadas aplicaciones. 

## Historia del Falcon 9: Logros y Fallas
Desde su anuncio en 2005 y posterior desarrollo y primer lanzamiento en 2010 hasta la actualidad, el Falcon 9 ha logrado importantes hitos, aunque también se deben mencionar algunos fallos.
- **2010**
    - Primer vuelo de un cohete Falcon 9 v1.0 junto a una cápsula Dragon de prueba.
- **2012**
    - El vuelo 3 puso en órbita a la primer cápsula Dragon en llegar a la EEI.
    - El vuelo 4 de Falcon 9 sufrió una falla parcial, logro llevar su carga principal (CRS-1) a la órbita correcta, pero la carga secundaria se insertó en una órbita anormalmente baja y se perdió. La falla se debió a que uno de los nueve motores Merlin se apagó durante el lanzamiento.
- **2013**
    - Primer lanzamiento de un Falcon 9 a la órbita GTO. 
- **2014**
    - Luego de entregar su carga (CRS-3) en una prueba de descenso controlado se logró el primer aterrizaje oceánico exitoso, pero luego la primer etapa se volcó y se destruyó.
- **2015**
    - En el vuelo 19 (CRS-7) el Falcon 9 v1.1 se desintegró durante el vuelo y la pérdida de la carga útil fue total. El fallo fue debido a una falla en el soporte de los tanques.
    - Luego de muchos intentos se logró el primer aterrizaje vertical en tierra firme de la primer etapa, convirtiendo al Falcon 9 en el primer cohete en aterrizar propulsivamente después de entregar una carga útil en órbita.
- **2016** 
    - Primer aterrizaje vertical en una barcaza en el mar de una primer etapa.
    - Durante una prueba (del vuelo 29) en una rampa de lanzamiento, la explosión de un Falcon 9 v1.2 destruyó el satélite Amos 6, debido a un fallo en la segunda etapa.
- **2017**
    - Primer Falcon 9  en llevar una carga útil con una primer etapa reutilizada y volver a aterrizar exitosamente. Primera vez en que se pudo recuperar el carenado de la carga útil (utilizado para proteger la carga útil durante el lanzamiento).
- **2018**
    - Primer lanzamiento del Falcon 9 Block 5 (última versión).
    - Fue lanzado el satélite Argentino de observación SAOCOM.
- **2019**
    - Primer vuelo de demostración del Spacex Crew Dragon (cápsula capaz de transportar personas).
- **2020**
    - Se recuperó por quinta vez una primer etapa (B1049), previamente lanzada en 2018.
    - Se lanzó por primera vez una misión tripulada hacia la EEI, en un Falcon 9 v1.2 Block 5 con una cápsula Dragon Crew y dos astronautas a bordo.

Toda la evolución que han experimentado los Falcon 9 tienen como resultado 85 lanzamientos, 53 etapas recuperadas y 36 reutilizadas.
## Conclusiones
El Falcon 9 es un cohete que ha hecho historia. A lo largo de su trayectoria ha mejorado enormemente su sistema de recuperación y reutilización de partes lo que marco un antes y un después en la industria aeroespacial. Entre las potenciales mejoras ingenieriles una es hacer que en un futuro la segunda etapa sea recuperable y reutilizable como la primer etapa. Además otro aspecto a mejorar es la combustión dado que los combustibles utilizados actualmente resultan contaminantes para el medio ambiente y en otro tipo de motores de Spacex se ha visto ese cambio de combustibles a unos menos dañinos (en los motores Raptor).  
Como futuras/os ingenieras/os mecánicas/os, consideramos que los conocimientos asociados con la física, química, matemática y programación entre otras, son indispensables y fundamentales para el desarrollo de tecnologías que nos permitan explorar la siguiente barrera que tiene que afrontar la humanidad: el espacio.

See you on Mars... 🚀

# Bibliografía
## Falcon 9 ## 

* [spaceX](https://www.spacex.com/)

* [Wikipedia: Falcon9 - SpaceX](https://en.wikipedia.org/wiki/Falcon_9)
## Sistema de aterrizaje

* [Wikipedia: Programa del sistema de lanzamiento reutilizable SpaceX](https://en.wikipedia.org/wiki/SpaceX_reusable_launch_system_development_program)

* [Naukas: Primer lanzamiento de un Falcon 9 con una etapa recuperada](https://danielmarin.naukas.com/2017/03/31/primer-lanzamiento-de-un-falcon-9-con-una-etapa-recuperada-ses-10/)

* [Youtube: Todo lo que debes saber sobre el Falcon 9 de SpaceX](https://www.youtube.com/watch?v=C9bANkdJO20&t=774s)

## Evolución motores Merlin

* [SpaceX Merlin (en inglés)](https://en.wikipedia.org/wiki/SpaceX_Merlin "SpaceX Merlin (en inglés)")

* [Merlín (motor cohete)](https://es.wikipedia.org/wiki/Merl%C3%ADn_(motor_cohete) "Merlín motor cohete")

* [Evolution of SpaceX Merline engine](http://www.b14643.de/Spacerockets_2/United_States_1/Falcon-9/Merlin/index.htm "Evolution of SpaceX Merline engine")  

##  Motor Merlin 1d

* [Wikipedia: Spacex - Merlin](https://en.wikipedia.org/wiki/SpaceX_Merlin#cite_note-spacexfalcon9Merlinpage-4)  

* [Primeras pruebas Merlin 1D](https://www.nasaspaceflight.com/2012/06/spacex-merlin-1d-orbital-fire-aj-26-engine/)  

* [Comparativa entre distintos motores de cohetes](https://www.nextbigfuture.com/2019/05/spacex-raptor-engine-will-be-best-on-cost-and-nearly-best-on-isp.html)  

* [Merlin y Raptor: los motores de cohete reutilizables que han llevado a SpaceX a ser referente espacial](https://www.xataka.com/espacio/merlin-raptor-motores-cohete-altamente-reutilizables-que-han-llevado-a-spacex-a-ser-referente-espacial)  

* [Youtube: Software powering Falcon 9 & Dragon - Simply Explained](https://www.youtube.com/watch?v=N5faA2MZ6jY)  

* [Youtube: Tom Mueller (SpaceX) Explains The Merlin Rocket Engine](https://www.youtube.com/watch?v=vVQyZn-VtXU)  

* [Youtube: How a Rocket works?](https://www.youtube.com/watch?v=QQB1Iw3zJbc)

## Inyector de pinza

* [Wikipedia: Pintle Injector](https://en.wikipedia.org/wiki/Pintle_injector "Pintle Injector") 

## Historia del Falcon 9

* [Wikipedia: List of Falcon 9 and Falcon Heavy launches](https://en.wikipedia.org/wiki/List_of_Falcon_9_and_Falcon_Heavy_launches)  

* [SpaceX Falcon 9 Data Sheet](http://www.spacelaunchreport.com/falcon9.html)   

* [Diez años del Falcon 9: octava misión Starlink, debut del VisorSat y quinta recuperación de una etapa](https://danielmarin.naukas.com/2020/06/04/octava-mision-starlink-debut-del-visorsat-y-quinta-recuperacion-de-una-etapa/)  

* [Primer lanzamiento tripulado de la Crew Dragon de SpaceX (misión DM-2)](https://danielmarin.naukas.com/2020/05/31/primer-lanzamiento-tripulado-de-la-crew-dragon-de-spacex-mision-dm-2/)  



