## División de tareas
- Descripción del vehículo de lanzamiento Falcon9, versiones y propósitos.**PAU**  
- Especificaciones del Merlín 1D: partes principales, aspectos técnicos
Merlín 1D: Aspectos del diseño y principio de funcionamiento. **FLOR**  
- Inyector de pinza: principio de funcionamiento y usos. **AGUS**  
- Revisión de las versiones del Merlin 1A a 1D. **AGUS**  
- Descripción del sistema de control de aterrizaje del Falcon 9. **PAU**  
- Línea de tiempo con logros y fallas del sistema de aterrizaje del Falcon. **FLOR**  
- Evaluación de las potenciales demandas de ingeniería en el desarrollo del Falcon y qué materias/conocimientos asociados en su carrera podrían ajustarse más a las mismas. **TODOS**  